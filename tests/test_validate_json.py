import imp

from ska_telmodel.schema import validate as ska_telmodel_validate
from ska_telmodel.csp import version, schema, make_csp_config
from ska_telmodel.csp.examples import get_csp_config_example
from ska_telmodel.csp.version import *

from ska.pst.util import validate, Strictness

import pytest
import json

# we need this to be on the component_manager, but for now have this
# as a separate class (this could be within the component manager)


def is_valid_combination(version: str, scan: str) -> bool:
    if scan is None or scan not in [
        "pst_beam",
        "pst_scan_pt",
        "pst_scan_ds",
        "pst_scan_ft",
    ]:
        return False

    return version.startswith(CSP_CONFIG_VER2_2)


@pytest.mark.parametrize(
    "version, valid, scan",
    [
        (v, is_valid_combination(v, scan), scan)
        for v in CSP_CONFIG_VERSIONS
        for scan in [
            None,
            "cal_a",
            "science_a",
            "pst_beam",
            "pst_scan_pt",
            "pst_scan_ds",
            "pst_scan_ft",
        ]
    ],
)
def test_only_version_2_2_or_above_accepted(
    version: str, valid: bool, scan: str
) -> None:
    try:
        request = get_csp_config_example(version=version, scan=scan)

        if valid:
            validate(request, strictness=Strictness.Strict)
        else:
            with pytest.raises(ValueError, match=r"should be >= 2.2"):
                validate(request, strictness=Strictness.Strict)
    except ValueError:
        pass


def test_should_fail_validation() -> None:
    config = {
        "interface": CSP_CONFIG_VER2_2,
        "pst": {
            "scan": {
                "foo": "bar",
            },
        },
    }

    with pytest.raises(ValueError):
        validate(config)
