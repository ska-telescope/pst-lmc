import pytest
import json

from ska_telmodel.schema import validate
from ska_telmodel.csp import version, schema, make_csp_config
from ska_telmodel.csp.examples import get_csp_config_example
from ska_telmodel.csp.version import *

from ska.pst.util import Strictness, Configuration


def test_should_deserialise_valid_json():
    example = get_csp_config_example(version=CSP_CONFIG_VER2_2, scan="pst_scan_pt")

    # serialise
    request = json.dumps(example)

    obj = Configuration.from_json(request)

    assert obj is not None


def test_should_add_default_values():
    example = get_csp_config_example(version=CSP_CONFIG_VER2_2, scan="pst_scan_pt")

    try:
        del example["pst"]["scan"]["coordinates"]["equinox"]
    except KeyError:
        pass

    request = json.dumps(example)

    obj = Configuration.from_json(request)

    assert (
        obj["pst"]["scan"]["coordinates"]["equinox"] == 2000.0
    ), "Equinox should have been added as default"


def test_to_json_should_create_json_string():
    import json

    values = {"foo": "bar"}
    config = Configuration(values=values)

    result = config.to_json()
    expected = json.dumps(values)

    assert result == expected, f"config.to_json '{result}' not the same as '{expected}'"


def test_should_fail_to_instantiate_if_values_is_none():
    with pytest.raises(ValueError, match=r"Parameter 'values' must not be empty"):
        Configuration(None)


def test_should_fail_to_instantiate_if_values_is_empty():
    with pytest.raises(ValueError, match=r"Parameter 'values' must not be empty"):
        Configuration({})


def test_get_attributes():
    example = get_csp_config_example(version=CSP_CONFIG_VER2_2, scan="pst_scan_pt")

    obj = Configuration(example)

    assert obj.pst == example["pst"]
    assert obj["pst"] == example["pst"]


def test_set_attributes():
    example = get_csp_config_example(version=CSP_CONFIG_VER2_2, scan="pst_scan_pt")

    # serialise
    request = json.dumps(example)

    obj = Configuration.from_json(request)

    obj["foo"] = "bar"
    obj.cat = "dog"

    assert obj.foo == "bar"
    assert obj["cat"] == "dog"


def test_delete_item():
    example = get_csp_config_example(version=CSP_CONFIG_VER2_2, scan="pst_scan_pt")
    print(example.keys())

    obj = Configuration(example)

    assert "pst" in example
    assert "pst" in obj._values
    assert obj.pst is not None
    assert "pst" in obj.keys()
    del obj["pst"]
    assert "pst" not in obj.keys()


def test_dict_methods():
    example = get_csp_config_example(version=CSP_CONFIG_VER2_2, scan="pst_scan_pt")

    obj = Configuration(example)

    assert len(obj) == len(example)
    assert obj.keys() == example.keys()
    assert [*obj.values()] == [*example.values()]
    assert obj.items() == example.items()

    for k, v in obj.items():
        assert k in example
        assert example[k] == v
