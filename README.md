SKA PST LMC
=====================

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-pst-lmc/badge/?version=latest)](https://readthedocs.org/projects/ska-telescope-pst-lmc/badge/?version=latest)

## About
The repository for the Local Monitoring and Control (LMC) software the Pulsar Timing Sub-element (PST) within the Central Signal Processor (CSP) element of the Square Kilometre Array (SKA) Low and Mid telescopes.

## TANGO Devices

This repository contains several TANGO classes that model the PST LMC functions. Devices that are designed to interact with a control process of a sub-element (i.e. RECV, SMRB, DSP, SEND) extend from `PstSoftwareDevice` to be able to reuse common functionality.

### PstMaster

This is the public device for PST, that proxies commands to the PstBeam and PstBeamSrvBmc devices. There is no actual logic in here except for routing information to the correct device.

### PstBeamSrvBmc

The Beam Server Baseband Management Controller TANGO device, it was designed to use for monitoring of the servers but this is now being centralised with SKA. In future this device may be obsolete.

### PstSoftwareDevice - Base TANGO class

This is the base class used by other devices, that is used to deal with common code functionality. It extends the SKABaseDevice class, which means anything that extends this also extends that class.

It provides an interface for the following commands:
| Command | Details |
|---------|---------|
| InitCommand | Used for device initialisation. Sets up device attributes like build_state, version_id, and software_version.<br> This also sets up a control socket used by the subclasses (see below) |
| CreateBeam | Used by clients when creating a beam. Allocates the required resources for the beam configuration |
| SetMode | Used to set the observing mode for the beam that has been configured. |
| UpdateSubint	| Set the observing mode for the beam |
| Terminate	| Implements the termination for the observing |
| DeleteBeam | Used for releasing resources for a configured beam. |

This class also provides a `ControlSocket` that is intended to be used by the sub-classes to talk to their appropriate control processes. This pattern is subject to change (see ????)

### PstBeam - Beam Logical TANGO Device

This TANGO device is a logical device that acts as the public interface for the PST sub-system. It is configured with a fully qualified domain name (FQDN) of other sub-elements of the PST system, such as RECV, SMRB, DSP, Stat, SEND, MGMT.

It provides the following commands that can be executed:
| Command | Notes |
|---------|-------|
| On | Used to switch on the PST devices allocated to the PST beam. Should only be called when AdminMode is ONLINE or MAINTENANCE (not OFFLIE, NOT-FITTED or RESERVERD)<br><br>Loops over the device proxies and calls the On command on them.|
| Off	| Used to switch off all the PST devices associated with the PST beam.<br><br>Loops over the device proxies and calls the Off command on them.|
| Reset | Used to call Reset on the PST devices.<br><br>Loops over the device proxies and calls the Reset Command on them|
| SetMode|Used to call CreateBeam and then SetMode on the associated device proxies.<br><br>First calls CreateBeam on all devices, before calling the SetMode.|
|UpdateSubInt	|Used to update the beam configuration during a Scan<br><br>Loops over the device proxies and calls the UpdateSubint Command on them|
|Terminate	|Used to call Terminate on the PST devices.<br><br>Loops over the device proxies and calls the Terminate Command on them|

### PstReceive - RECV TANGO Device

This is a softare TANGO device that is used for the RECV process within PST.

### PstSmrb - SMRB TANGO Device

This is the software TANGO device used for managing the shared memory ring buffers (SMRB) within PST.

### PstDsp - DSP TANGO Device

This is the software TANGO device used for managing the digital signal processing (DSP) process within PST

## Version History
The first version of the PST LMC classes is yet to be released.

## Installation

### Requirements
A TANGO development environment properly configured, as described in [SKA developer portal - Tango Development Environment set up](https://developer.skatelescope.org/en/latest/getting-started/devenv-setup/tango-devenv-setup.html)
- [SKA Tango Base Classes and Utilities](https://gitlab.com/ska-telescope/ska-tango-base)
- [CPS.LMC Common](https://gitlab.com/ska-telescope/ska-csp-lmc-common)

## Repository Organisation
The `PST.LMC` repository is organized in a single code tree. The hierarchy contains:
- _src/ska/pst/lmc_: contains the specific project TANGO Device Class LMC files
- _src/ska/pst/util_: contains non-TANGO classes that can be used across project
- _pogo_: contains the POGO files of the TANGO Device Classes of the project
- _charts_: contains the `docker`, `docker-compose` and `dsconfig` configuration files as well as the Makefile to generate the docker image and run the tests.
- _tests_: contains the unit tests. Tests in here don't require TANGO to be running and should be able to be run on a developer's own machine. This includes not having to have Docker/Kubernetes running.
- _post-deployment/tests_: contains tests that are to be run after a Docker deployment, as they require TANGO infrastructure to be up and running.

## Running Tests

Running unit tests requires that the `development-requirements.txt` has been installed. Within your Python environment make sure that these packages are installed.

```
$ pip install -r development-requirements.txt
```

To run the unit tests, run the following command:

```
$ PYTHONPATH=src pytest tests
```

**Note**: that once this repository has been upgraded to work with the SKA development environment the way of running of tests will be different.

## Read The Docs Integration

SKA requires that the software is self documenting and that as part of the build process that documentation for the project is generated and published. The documentation is generated using `sphinx`. To generate the documentation locally use

```
$ cd docs/src
$ sphinx-build -T -E -d _build/doctrees-readthedocs -D language=en . _build/html -W
```

The docs can then be found in the `_build/html` directory. VS Code can provide a `Live Server` view of this to check that
the documentation is being generated correctly.

## Confluence Documentation

Design and architecture decisions about the PST-LMC TANGO devices is captured in Confluence. Check the
[PST TANGO Devices](https://confluence.skatelescope.org/display/SE/PST+TANGO+Devices) landing page for links to specific
documentation.

## Troubleshooting
TBD

## License
See the LICENSE file for details.


Notes
-----

1. The ``coverage`` module interferes with PyDev debugging. To work around this
problem, comment out the pytest ``addopts`` section in setup.cfg.

1. The tests in ``post-deployment/tests/test_1_test_server_using_devicetestcontext.py``
   execute using the Tango DeviceTestContext helper. These tests create a new
   device per test, requiring that each DeviceTestContext run in a new process
   to avoid SegmentationFault errors. For more info, see:
   * https://github.com/tango-controls/pytango/pull/77
   * http://www.tango-controls.org/community/forum/c/development/python/testing-tango-devices-using-pytest/?page=1#post-3761

   We should form a recommendation on whether a new device should be formed per test or not.

1. Running DeviceTestContext tests _after_ tests using a Tango client results in
   errors when the DeviceTestContext gets stuck in initialisation. As a workaround, the test filenames are named in so that the DeviceTestContext tests run first.

   We should form a recommendation on what tests are expected: should projects
   just contain unit tests using DeviceTestContext, or should they also contain
   integration tests using Tango client? When unit testing using DeviceTestContext,
   how should other devices be mocked?
