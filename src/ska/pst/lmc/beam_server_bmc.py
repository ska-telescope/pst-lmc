# -*- coding: utf-8 -*-
#
# This file is part of the PstBeamSrvBmc project
#
# Swinburne University of Technology 2020
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" pst-lmc

Pulsar Timing Beam Server Baseband Management Controller TANGO device
"""

# PyTango imports
import tango
from tango.server import attribute
from tango.server import device_property

# Additional import
# PROTECTED REGION ID(PstBeamSrvBmc.additionnal_import) ENABLED START #
from ska.base import SKABaseDevice
from ska.base.commands import ResultCode
import subprocess
from csv import DictReader
from os import path

# PROTECTED REGION END #    //  PstBeamSrvBmc.additionnal_import

__all__ = ["PstBeamSrvBmc", "main"]


class PstBeamSrvBmc(SKABaseDevice):
    """
    Pulsar Timing Beam Server Baseband Management Controller TANGO device

    **Properties:**

    - Device Property
        BmcDeviceAddress
            - Address of the Baseband Management Controller. If the
              value may be a valid IPv4 address or a linux kernel device
            - Type:'DevString'
        BmcDeviceUser
            - Username to use for access to the Baseband Management
              Controller when the BmcDeviceAddress is a valid IPv4
              address
            - Type:'DevString'
        BmcDevicePassword
            - Password to access the BmcDeviceAddress in IPv4 mode
            - Type:'DevString'
    """

    _ipmi_cmd = "None"
    _ipmi_fieldnames = [
        "index",
        "value",
        "units",
        "state",
        "lower_non_recoverable",
        "lower_critical",
        "lower_non_critical",
        "upper_non_recoverable",
        "upper_critical",
        "upper_non_critical",
    ]
    _cpu1 = 0.0
    _cpu2 = 0.0
    _psu = False
    _fan1 = 0.0
    _fan2 = 0.0
    _fan3 = 0.0
    _fan4 = 0.0

    # PROTECTED REGION END #    //  PstBeamSrvBmc.class_variable

    # -----------------
    # Device Properties
    # -----------------

    BmcDeviceAddress = device_property(dtype="DevString", default_value="/dev/ipmi0")

    BmcDeviceUser = device_property(dtype="DevString", default_value="admin")

    BmcDevicePassword = device_property(dtype="DevString", default_value="password")

    # ----------
    # Attributes
    # ----------

    # ---------------
    # General methods
    # ---------------

    class InitCommand(SKABaseDevice.InitCommand):
        """
        Command class for device initialisation
        """

        def do(self):
            """
            Stateless hook for initialisation of the attributes and
            properties of the PstBeamSrvBmc.
            """
            import ska.pst.lmc.release as release

            (result_code, message) = super().do()

            # self.logger.info("self.set_state(DevState.INIT)")
            # self.set_state(DevState.INIT)

            device = self.target
            device._build_state = release.get_release_info()
            device._version_id = release.VERSION
            device._software_version = release.VERSION

            device._cpu1 = 0.0
            device._cpu2 = 0.0
            device._psu = False
            device._fan1 = 0.0
            device._fan2 = 0.0
            device._fan3 = 0.0
            device._fan4 = 0.0
            device._ipmi_cmd = "None"

            device.set_change_event("adminMode", True, True)
            device.set_archive_event("adminMode", True, True)

            return (result_code, message)

    def init_command_objects(self):
        """
        Initialises the command handlers for commands supported by this
        device.
        """

        self.logger.info("super().init_command_objects()")
        super().init_command_objects()

        args = (self, self.state_model, self.logger)

        self.logger.info("Registering PST specific commands")
        self.register_command_object("On", self.OnCommand(*args))
        self.register_command_object("Off", self.OffCommand(*args))

    def simulate_ipmitool(self):
        """
        Set the IPMI monitored values to sensible defaults
        """
        self.logger.debug("simulating IPMITOOL values")
        self._cpu1 = 21
        self._cpu2 = 22
        self._psu = True
        self._fan1 = 1100
        self._fan2 = 1200
        self._fan3 = 1300
        self._fan4 = 1400

    def always_executed_hook(self):
        """
        Method always executed before any TANGO command is executed.
        """
        if self.BmcDeviceAddress.startswith("/dev/ipmi"):
            if path.exists(self.BmcDeviceAddress):
                self._ipmi_cmd = "ipmitool"
            else:
                self._ipmi_cmd = "None"
        else:
            self._ipmi_cmd = (
                "ipmitool -I lanplus -H "
                + self.BmcDeviceAddress
                + " -U "
                + self.BmcDeviceUser
                + " -P "
                + self.BmcDevicePass
            )

        if self._ipmi_cmd == "None":
            self.simulate_ipmitool()
            return

        args = [self._ipmi_cmd, "sensor", "list"]
        result = subprocess.run(args, check=False, stdout=subprocess.PIPE)
        if result.returncode == 0:
            data = result.stdout.decode("utf-8").split("\n")
            for row in DictReader(
                data, fieldnames=self._ipmi_fieldnames, delimiter="|"
            ):
                index = row["index"].rstrip()
                value = row["value"].rstrip()
                self.set_ipmi_attribute(index, value)

    def set_ipmi_attribute(self, index, value):
        """
        Convert the string value to the correct type and assign it to the
        matching IPMI attribute"
        """
        if index == "CPU1 Temp":
            self._cpu1 = float(value)
        if index == "CPU2 Temp":
            self._cpu2 = float(value)
        if index == "PSU":
            self._psu = bool(value)
        if index == "Fan 1":
            self._fan1 = bool(value)
        if index == "Fan 2":
            self._fan2 = bool(value)
        if index == "Fan 3":
            self._fan3 = bool(value)
        if index == "Fan 4":
            self._fan4 = bool(value)

    def delete_device(self):
        """
        Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """

    # ------------------
    # Attributes methods
    # ------------------

    @attribute(
        dtype="DevDouble",
        label="CPU1 Temp",
        unit="degrees C",
        display_unit="C",
        polling_period=5000,
        max_value=95,
        min_value=-11,
        max_alarm=90,
        min_alarm=-7,
        max_warning=85,
        min_warning=-5,
        doc="Temperature for CPU1",
    )
    def cpu1(self):
        """
        Return the cpu1 attribute.
        """
        return self._cpu1

    @attribute(
        dtype="DevDouble",
        label="CPU2 Temp",
        unit="degrees C",
        display_unit="C",
        polling_period=5000,
        max_value=95,
        min_value=-11,
        max_alarm=90,
        min_alarm=-7,
        max_warning=85,
        min_warning=-5,
        doc="Temperature for CPU2",
    )
    def cpu2(self):
        """
        Return the cpu2 attribute.
        """
        return self._cpu2

    @attribute(
        dtype="DevBoolean",
        label="PSU",
        polling_period=5000,
        doc="Power supply subsystem",
    )
    def psu(self):
        """
        Return the psu attribute.
        """
        return self._psu

    @attribute(
        dtype="DevDouble",
        label="Fan 1",
        unit="RPM",
        standard_unit="RPM",
        display_unit="RPM",
        polling_period=5000,
        max_value=34425,
        min_value=405,
        max_alarm=34290,
        min_alarm=540,
        max_warning=34115,
        min_warning=675,
        doc="Rotation rate for cooling fan 1",
    )
    def fan1(self):
        """
        Return the fan1 attribute.
        """
        return self._fan1

    @attribute(
        dtype="DevDouble",
        label="Fan 2",
        unit="RPM",
        standard_unit="RPM",
        display_unit="RPM",
        polling_period=5000,
        max_value=34425,
        min_value=405,
        max_alarm=34290,
        min_alarm=540,
        max_warning=34115,
        min_warning=675,
        doc="Rotation rate for cooling fan 2",
    )
    def fan2(self):
        """
        Return the fan2 attribute.
        """
        return self._fan2

    @attribute(
        dtype="DevDouble",
        label="Fan 3",
        unit="RPM",
        standard_unit="RPM",
        display_unit="RPM",
        polling_period=5000,
        max_value=34425,
        min_value=405,
        max_alarm=34290,
        min_alarm=540,
        max_warning=34115,
        min_warning=675,
        doc="Rotation rate for cooling fan 3",
    )
    def fan3(self):
        """
        Return the fan3 attribute.
        """
        return self._fan3

    @attribute(
        dtype="DevDouble",
        label="Fan 4",
        unit="RPM",
        standard_unit="RPM",
        display_unit="RPM",
        polling_period=5000,
        max_value=34425,
        min_value=405,
        max_alarm=34290,
        min_alarm=540,
        max_warning=34115,
        min_warning=675,
        doc="Rotation rate for cooling fan 4",
    )
    def fan4(self):
        """
        Return the fan4 attribute.
        """
        return self._fan4

    # --------
    # Commands
    # --------

    class OnCommand(SKABaseDevice.OnCommand):
        """
        Command class for the On() command
        """

        def do(self):
            """
            Stateless hook implementing the functionality of the
            On command.

            Switch on the PST devices allocated to the PST beam. The command is
            executed if the AdminMode is ONLINE or MAINTENANCE. If the
            AdminMode is OFFLINE, NOT-FITTED or RESERVED, the method
            throws an exception

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            :rtype: (ResultCode, str)
            """
            device = self.target
            self.logger.info("ipmi_cmd=%s", device._ipmi_cmd)
            if device._ipmi_cmd == "None":
                # support key state transitions when hardware control is unavailable
                (result_code, message) = super().do()
                return (result_code, message)

            args = [device._ipmi_cmd, "power", "on"]
            result = subprocess.run(args, check=False, stdout=subprocess.PIPE)
            if result.returncode == 0:
                (result_code, message) = super().do()
                return (result_code, message)

            self.logger.warning(result.stdout.decode("utf-8"))
            # transition to the unknown state
            device.set_state(tango.DevState.UNKNOWN)

            # do not call the state transition
            # (result_code, message) = super().do()

            # PST specific stuff goes here
            return (
                ResultCode.FAILED,
                "ipmitool failed to turn on the device: %s",
                result.stdout.decode("utf-8"),
            )

    class OffCommand(SKABaseDevice.OffCommand):
        """
        Command class for the Off() command
        """

        def do(self):
            """
            Stateless hook implementing the functionality of the
            Off command.

            Switch off all the PST devices associated with the PST beam.

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            :rtype: (ResultCode, str)
            """
            device = self.target

            if device._ipmi_cmd == "None":
                # support key state transitions when hardware control is unavailable
                (result_code, message) = super().do()
                return (result_code, message)

            args = [device._ipmi_cmd, "power", "off"]
            result = subprocess.run(args, check=False, stdout=subprocess.PIPE)
            if result.returncode == 0:
                (result_code, message) = super().do()
                return (result_code, message)

            self.logger.warning(result.stdout.decode("utf-8"))
            device.set_state(tango.DevState.UNKNOWN)
            return (
                ResultCode.FAILED,
                "ipmitool failed to turn off the device: %s",
                result.stdout.decode("utf-8"),
            )


# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    """Main function of the PstBeamSrvBmc module."""
    # PROTECTED REGION ID(PstBeamSrvBmc.main) ENABLED START #
    return PstBeamSrvBmc.run_server(args=args, **kwargs)
    # PROTECTED REGION END #    //  PstBeamSrvBmc.main


if __name__ == "__main__":
    main()
