# -*- coding: utf-8 -*-
#
# This file is part of the PstSoftwareDevice project
#
#
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" SKA Pulsar Timing Sub-element

Abstract base class for functionality common to all Pst Software Devices
"""

# PyTango imports
# pylint: disable=unused-import,duplicate-code,no-member
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
import enum

# pylint: enable=unused-import,duplicate-code
# Additional import
from ska.base import SKABaseDevice
from ska.base.commands import ResponseCommand
from ska.base.control_model import SimulationMode

__all__ = ["PstSoftwareDevice", "main"]


class PstSoftwareDevice(SKABaseDevice):
    """
    Abstract base class for functionality common to all Pst Software Devices

    **Properties:**

    - Device Property
    """

    # -----------------
    # Device Properties
    # -----------------
    IPAddress = device_property(dtype="DevString", default_value="127.0.0.1")

    Port = device_property(dtype="DevInt", default_value=12345)

    # ----------
    # Attributes
    # ----------

    # ---------------
    # General methods
    # ---------------

    class InitCommand(SKABaseDevice.InitCommand):
        """
        Command class for device initialisation
        """

        def do(self):
            """
            Stateless hook for initialisation of the attributes and
            properties of the PstSoftwareDevice.
            """
            from .util import ControlSocket
            import ska.pst.lmc.release as release

            (result_code, message) = super().do()

            # self.logger.info("self.set_state(DevState.INIT)")
            # self.set_state(DevState.INIT)

            device = self.target
            device._build_state = release.get_release_info()
            device._version_id = release.VERSION
            device._software_version = release.VERSION

            device.set_change_event("adminMode", True, True)
            device.set_archive_event("adminMode", True, True)

            # assume simulation mode. TODO how to configure this properly
            device._simulation_mode = SimulationMode.TRUE

            # configure the client control socket
            device._control_socket = ControlSocket(device.IPAddress, device.Port)

            return (result_code, message)

    def init_command_objects(self):
        """
        Initialises the command handlers for commands supported by this
        device.
        """

        self.logger.info("super().init_command_objects()")
        super().init_command_objects()

        args = (self, self.state_model, self.logger)

        self.logger.info("Registering PST specific commands")
        self.register_command_object("CreateBeam", self.CreateBeamCommand(*args))
        self.register_command_object("SetMode", self.SetModeCommand(*args))
        self.register_command_object("UpdateSubint", self.UpdateSubintCommand(*args))
        self.register_command_object("Stop", self.StopCommand(*args))
        self.register_command_object("Terminate", self.TerminateCommand(*args))
        self.register_command_object("DeleteBeam", self.DeleteBeamCommand(*args))
        self.register_command_object("SelfTest", self.SelfTestCommand(*args))

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # ensure the control socket to the software device is closed
        self._control_socket.close()

    # ------------------
    # Attributes methods
    # ------------------

    # --------
    # Commands
    # --------

    class CreateBeamCommand(ResponseCommand):
        """
        Class for handling the CreateBeam() command.
        """

        def do(self, argin=None):
            """
            Implementation of CreateBeam() command functionality.

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            :rtype: (ResultCode, str)
            """
            device = self.target
            device.logger.info("argin=[%s]", argin)
            argout = "{}"
            if device._simulation_mode != SimulationMode.TRUE:
                argout = device._control_socket.json_command("CreateBeam", argin)
            (return_code, message) = device.create_beam(argin, argout)
            return (return_code, message)

    @command(
        dtype_in="DevString",
        doc_in="Configuration parameters in JSON string",
        dtype_out="DevVarLongStringArray",
        doc_out="(ResultCode, 'informational message')",
    )
    @DebugIt()
    def CreateBeam(self, argin):
        """
        Allocate resources required for the specific beam configuration.

        :param argin: Configuration parameters in JSON string
        :type argin: 'DevString'

        :return: (ResultCode, 'informational message')
        """
        handler = self.get_command_object("CreateBeam")
        (return_code, message) = handler(argin)
        return [[return_code], [message]]

    class SetModeCommand(ResponseCommand):
        """
        Class for handling the SetMode() command.
        """

        def do(self, argin=None):
            """
            Implementation of SetMode() command functionality.

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            :rtype: (ResultCode, str)
            """
            device = self.target
            argout = "{}"
            if device._simulation_mode != SimulationMode.TRUE:
                argout = device._control_socket.json_command("SetMode", argin)
            (return_code, message) = device.set_mode(argin, argout)
            return (return_code, message)

    @command(
        dtype_in="DevString",
        doc_in="Configuration parameters in JSON string",
        dtype_out="DevVarLongStringArray",
        doc_out="(ResultCode, 'informational message')",
    )
    @DebugIt()
    def SetMode(self, argin):
        """
        Set the observing mode for the beam

        :param argin: Configuration parameters in JSON string
        :type argin: 'DevString'

        :return: (ResultCode, 'informational message')
        """
        handler = self.get_command_object("SetMode")
        self.logger.debug("setting mode with argin=%s", argin)
        (return_code, message) = handler(argin)
        return [[return_code], [message]]

    class UpdateSubintCommand(ResponseCommand):
        """
        Class for handling the UpdateSubint() command.
        """

        def do(self, argin=None):
            """
            Implementation of UpdateSubint() command functionality.

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            :rtype: (ResultCode, str)
            """
            device = self.target
            argout = "{}"
            if device._simulation_mode != SimulationMode.TRUE:
                argout = device._control_socket.json_command("UpdateSubint", argin)
            (return_code, message) = device.update_subint(argin, argout)
            return (return_code, message)

    @command(
        dtype_in="DevString",
        doc_in="Configuration parameters in JSON string",
        dtype_out="DevVarLongStringArray",
        doc_out="(ResultCode, 'informational message')",
    )
    @DebugIt()
    def UpdateSubint(self, argin):
        """
        Set the observing mode for the beam

        :param argin: Configuration parameters in JSON string
        :type argin: 'DevString'

        :return: (ResultCode, 'informational message')
        """
        handler = self.get_command_object("UpdateSubint")
        self.logger.debug("setting mode with argin=%s", argin)
        (return_code, message) = handler(argin)
        return [[return_code], [message]]

    class StopCommand(ResponseCommand):
        """
        Class for handling the Stop() command.
        """

        def do(self, argin=None):
            """
            Implementation of Stop() command functionality.

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            :rtype: (ResultCode, str)
            """
            device = self.target
            argout = "{}"
            if device._simulation_mode != SimulationMode.TRUE:
                argout = device._control_socket.json_command("Stop", argin)
            (return_code, message) = device.stop(argin, argout)
            return (return_code, message)

    @command(
        dtype_in="DevString",
        doc_in="Configuration parameters in JSON string",
        dtype_out="DevVarLongStringArray",
        doc_out="(ResultCode, 'informational message')",
    )
    @DebugIt()
    def Stop(self, argin):
        """
        Set the observing mode for the beam

        :param argin: Configuration parameters in JSON string
        :type argin: 'DevString'

        :return: (ResultCode, 'informational message')
        """
        handler = self.get_command_object("Stop")
        self.logger.debug("setting mode with argin=%s", argin)
        (return_code, message) = handler(argin)
        return [[return_code], [message]]

    class TerminateCommand(ResponseCommand):
        """
        Class for handling the Terminate() command.
        """

        def do(self, argin=None):
            """
            Implementation of Terminate() command functionality.

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            :rtype: (ResultCode, str)
            """
            device = self.target
            argout = "{}"
            if device._simulation_mode != SimulationMode.TRUE:
                argout = device._control_socket.json_command("Terminate", argin)
            (return_code, message) = device.terminate(argin, argout)
            return (return_code, message)

    @command(
        dtype_in="DevString",
        doc_in="Configuration parameters in JSON string",
        dtype_out="DevVarLongStringArray",
        doc_out="(ResultCode, 'informational message')",
    )
    @DebugIt()
    def Terminate(self, argin):
        """
        Set the observing mode for the beam

        :param argin: Configuration parameters in JSON string
        :type argin: 'DevString'

        :return: (ResultCode, 'informational message')
        """
        handler = self.get_command_object("Terminate")
        self.logger.debug("setting mode with argin=%s", argin)
        (return_code, message) = handler(argin)
        return [[return_code], [message]]

    class DeleteBeamCommand(ResponseCommand):
        """
        Class for handling the DeleteBeam() command.
        """

        def do(self, argin=None):
            """
            Implementation of DeleteBeam() command functionality.

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            :rtype: (ResultCode, str)
            """
            device = self.target
            self.logger.debug("argin=[%s]", argin)
            argout = "{}"
            if device._simulation_mode != SimulationMode.TRUE:
                argout = device._control_socket.json_command("DeleteBeam", argin)
            self.logger.debug("argout=[%s]", argout)
            (return_code, message) = device.delete_beam(argin, argout)
            return (return_code, message)

    @command(
        dtype_in="DevString",
        doc_in="Configuration parameters in JSON string",
        dtype_out="DevVarLongStringArray",
        doc_out="(ResultCode, 'informational message')",
    )
    @DebugIt()
    def DeleteBeam(self, argin):
        """
        Set the observing mode for the beam

        :param argin: Configuration parameters in JSON string
        :type argin: 'DevString'

        :return: (ResultCode, 'informational message')
        """
        handler = self.get_command_object("DeleteBeam")
        self.logger.debug("setting mode with argin=%s", argin)
        (return_code, message) = handler(argin)
        return [[return_code], [message]]

    class SelfTestCommand(ResponseCommand):
        """
        Class for handling the SelfTest() command.
        """

        def do(self, argin=None):
            """
            Implementation of SelfTest() command functionality.

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            :rtype: (ResultCode, str)
            """
            device = self.target
            argout = "{}"
            if device._simulation_mode != SimulationMode.TRUE:
                argout = device._control_socket.json_command("SelfTest", argin)
            (return_code, message) = device.self_test(argin, argout)
            return (return_code, message)

    @command(
        dtype_in="DevString",
        doc_in="Configuration parameters in JSON string",
        dtype_out="DevVarLongStringArray",
        doc_out="(ResultCode, 'informational message')",
    )
    @DebugIt()
    def SelfTest(self, argin):
        """
        Perform available self testing prior to beam creation

        :param argin: Configuration parameters in JSON string
        :type argin: 'DevString'

        :return: (ResultCode, 'informational message')
        """
        handler = self.get_command_object("SelfTest")
        self.logger.debug("setting mode with argin=%s", argin)
        (return_code, message) = handler(argin)
        return [[return_code], [message]]


# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    """Main function of the PstSoftwareDevice module."""
    # PROTECTED REGION ID(PstSoftwareDevice.main) ENABLED START #
    return run((PstSoftwareDevice,), args=args, **kwargs)
    # PROTECTED REGION END #    //  PstSoftwareDevice.main


if __name__ == "__main__":
    main()
