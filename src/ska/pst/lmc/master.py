# -*- coding: utf-8 -*-
#
# This file is part of the PstMaster project
#
#
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" Pulsar Timing Sub-element Master

"""

# PyTango imports
# pylint: disable=unused-import,duplicate-code
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
import enum

# pylint: enable=unused-import,duplicate-code

# Additional import
# PROTECTED REGION ID(PstMaster.additionnal_import) ENABLED START #
from cspse.lmc.subelement_master import CspSubElementMaster
from ska.base.control_model import AdminMode
import ska.pst.lmc.release as release

# PROTECTED REGION END #    //  PstMaster.additionnal_import

__all__ = ["PstMaster", "main"]


class PstMaster(CspSubElementMaster):
    """

    **Properties:**

    - Device Property
        BeamFQDN
            - Address of the Beam capability TANGO Device
            - Type:'DevString'
        BeamServerFQDN
            - Address of the BeamServer TANGO device
            - Type:'DevString'
    """

    # PROTECTED REGION ID(PstMaster.class_variable) ENABLED START #
    _proxy_beam = None
    _proxy_beam_server = None
    _admin_mode = None
    _build_state = None
    _version_id = None

    # PROTECTED REGION END #    //  PstMaster.class_variable

    # -----------------
    # Device Properties
    # -----------------

    BeamFQDN = device_property(dtype="DevString", mandatory=True)

    BeamServerFQDN = device_property(dtype="DevString", mandatory=True)

    # ----------
    # Attributes
    # ----------

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the PstMaster."""
        CspSubElementMaster.init_device(self)
        # PROTECTED REGION ID(PstMaster.init_device) ENABLED START #

        # begin initialization
        self.set_state(DevState.INIT)

        self._admin_mode = AdminMode.MAINTENANCE

        self._build_state = release.get_release_info()
        self._version_id = release.VERSION

        # configure the TANGO device proxy for 1 PST Beam
        self._proxy_beam = tango.DeviceProxy(self.BeamFQDN)
        self._proxy_beam_server = tango.DeviceProxy(self.BeamServerFQDN)

        # transition to the On state
        self.set_state(tango.DevState.OFF)
        # PROTECTED REGION END #    //  PstMaster.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(PstMaster.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  PstMaster.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(PstMaster.delete_device) ENABLED START #
        # self._proxy_beam.command_inout("Off")
        # self._proxy_beam_server.command_inout("Off")
        self.set_state(tango.DevState.OFF)
        # PROTECTED REGION END #    //  PstMaster.delete_device

    # ------------------
    # Attributes methods
    # ------------------

    # --------
    # Commands
    # --------

    @command()
    @DebugIt()
    def Reset(self):
        # PROTECTED REGION ID(PstMaster.Reset) ENABLED START #
        """
        Reset device to its default state

        :return:None
        """
        # PROTECTED REGION END #    //  PstMaster.Reset

    @command(
        dtype_in="DevVarStringArray",
        doc_in="The list of sub-element components FQDNs to switch-on or an empty "
        "list to switch-on the whole CSP Sub-element."
        "                    "
        "If the array length is 0, the command applies to the whole CSP Sub-Element. If the "
        "array length is > 1, each array element specifies the FQDN of the"
        "CSP SubElement component to switch ON.",
    )
    @DebugIt()
    def On(self, argin):
        # PROTECTED REGION ID(PstMaster.On) ENABLED START #
        """
            Switch-on the CSP sub-element components specified by the input argument. If no
            argument is specified, the command is issued on all the CSP sub-element components.
            The command is executed if the *AdminMode* is ONLINE or *MAINTENANCE*.
            If the AdminMode is *OFFLINE*, *NOT-FITTED* or *RESERVED*, the method throws an
            exception.

        :param argin: 'DevVarStringArray'
            The list of sub-element components FQDNs to switch-on or an empty list to
            switch-on the whole CSP Sub-element.
            If the array length is 0, the command applies to the whole CSP Sub-Element.
            If the array length is > 1, each array element specifies the FQDN of the
            CSP SubElement component to switch ON.

        :return:None
        """
        self.logger.debug("Processing On command argin=%s", argin)
        self._proxy_beam.command_inout("On")
        self._proxy_beam_server.command_inout("On")
        # super().On(argin)
        # self.set_state(tango.DevState.ON)
        # PROTECTED REGION END #    //  PstMaster.On

    @command(
        dtype_in="DevVarStringArray",
        doc_in="If the array length is 0, the command applies to the whole"
        "CSP Sub-element."
        "If the array length is > 1, each array element specifies the FQDN of the"
        "CSP SubElement component to switch OFF.",
    )
    @DebugIt()
    def Off(self, argin):
        # PROTECTED REGION ID(PstMaster.Off) ENABLED START #
        """
            Switch-off the CSP sub-element components specified by the input argument.
            If no argument is specified, the command is issued to all the CSP
            sub-element components.

        :param argin: 'DevVarStringArray'
            If the array length is 0, the command applies to the whole
            CSP Sub-element.
            If the array length is > 1, each array element specifies the FQDN of the
            CSP SubElement component to switch OFF.

        :return:None
        """
        self.logger.debug("Processing Off command argin=%s", argin)
        self._proxy_beam.command_inout("Off")
        self._proxy_beam_server.command_inout("Off")
        # super().Off(argin)
        # self.set_state(tango.DevState.OFF)
        # PROTECTED REGION END #    //  PstMaster.Off

    @command(
        dtype_in="DevVarStringArray",
        doc_in="If the array length is 0, the command applies to the whole"
        "CSP sub-element."
        "If the array length is > 1, each array element specifies the FQDN of the"
        "CSP SubElement icomponent to put in STANDBY mode.",
    )
    @DebugIt()
    def Standby(self, argin):
        # PROTECTED REGION ID(PstMaster.Standby) ENABLED START #
        """
            Transit the CSP Sub-element or one or more CSP SubElement components from ON/OFF to
            STANDBY.

        :param argin: 'DevVarStringArray'
            If the array length is 0, the command applies to the whole
            CSP sub-element.
            If the array length is > 1, each array element specifies the FQDN of the
            CSP SubElement icomponent to put in STANDBY mode.

        :return:None
        """
        # super().Standby(argin)
        # PROTECTED REGION END #    //  PstMaster.Standby

    @command()
    @DebugIt()
    def Upgrade(self):
        # PROTECTED REGION ID(PstMaster.Upgrade) ENABLED START #
        """

        :return:None
        """
        # PROTECTED REGION END #    //  PstMaster.Upgrade


# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    """Main function of the PstMaster module."""
    # PROTECTED REGION ID(PstMaster.main) ENABLED START #
    return PstMaster.run_server(args=args, **kwargs)
    # PROTECTED REGION END #    //  PstMaster.main


if __name__ == "__main__":
    main()
