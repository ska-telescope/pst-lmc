# -*- coding: utf-8 -*-
#
# This file is part of the PstRecv project
#
#
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" SKA Pulsar Timing Sub-element

Software class modelling the RECV software component
"""

# PyTango imports
# pylint: disable=unused-import,no-member
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
import enum
import json
from random import randint

# pylint: enable=unused-import
# Additional import
from ska.base.control_model import SimulationMode
from ska.base.commands import ResultCode
from ska.pst.lmc.software_device import PstSoftwareDevice

__all__ = ["PstRecv", "main"]


class PstRecv(PstSoftwareDevice):
    """
    Software class modelling the RECV software component

    **Properties:**

    - Device Property
    """

    # -----------------
    # Device Properties
    # -----------------

    # ----------
    # Attributes
    # ----------

    _received_data = 0
    _received_rate = 0
    _dropped_data = 0
    _dropped_rate = 0
    _nchan = 0
    _misordered_packets = 0
    _malformed_packets = 0
    _relative_weights = []
    _relative_weight = 0

    # ---------------
    # General methods
    # ---------------

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        state = self.get_state()
        self.logger.debug("state=%s", str(state))
        if state in [DevState.OFF or DevState.INIT]:
            self.logger.debug("No action since state is off/init")
        else:
            if self._simulation_mode == SimulationMode.TRUE:
                self._simulate_recv()
            else:
                argout = self._control_socket.json_command("GetState", "{}")
                out = json.loads(argout)
                self._received_data = out["received_data"]
                self._received_rate = out["received_rate"]
                self._dropped_data = out["dropped_data"]
                self._dropped_rate = out["dropped_rate"]
                self._misordered_packets = out["misordered_packets"]
                self._malformed_packets = out["malformed_packets"]
                self._relative_weights = out["relative_weights"]
                self._relative_weight = out["relative_weight"]

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """

    # ------------------
    # Attributes methods
    # ------------------

    @attribute(
        dtype="DevFloat",
        unit="Gigabits per second",
        standard_unit="Gigabits per second",
        display_unit="Gb/s",
        polling_period=5000,
        max_value=200,
        min_value=0,
        doc="Current data receive rate from the CBF interface",
    )
    def received_rate(self):
        """
        Return the received data rate"
        """
        return self._received_rate

    @attribute(
        dtype="DevULong64",
        unit="Bytes",
        standard_unit="Bytes",
        display_unit="B",
        polling_period=5000,
        doc="Total number of bytes received from the CBF in the current scan",
    )
    def received_data(self):
        """
        Return the data received
        """
        return self._received_data

    @attribute(
        dtype="DevFloat",
        label="Drop Rate",
        unit="Megabits per second",
        standard_unit="Megabits per second",
        display_unit="Mb/s",
        polling_period=5000,
        max_value=200,
        min_value=-1,
        max_alarm=10,
        min_alarm=-1,
        max_warning=1,
        min_warning=-1,
        doc="Current rate of CBF ingest data being dropped or lost by the receiving process",
    )
    def dropped_rate(self):
        """
        Return the dropped data rate"
        """
        return self._dropped_rate

    @attribute(
        dtype="DevULong64",
        label="Dropped",
        unit="Bytes",
        standard_unit="Bytes",
        display_unit="B",
        polling_period=5000,
        doc="Total number of bytes dropped in the current scan",
    )
    def dropped_data(self):
        """
        Return the data dropped
        """
        return self._dropped_data

    @attribute(
        dtype="DevULong64",
        polling_period=5000,
        label="Out of order packets",
        doc="Total number of packets received out of order in the current scan",
    )
    def misordered_packets(self):
        """
        Return the number of misordered packets
        """
        return self._misordered_packest

    @attribute(
        dtype="DevULong64",
        polling_period=5000,
        min_value=0,
        min_alarm=-1,
        min_warning=-1,
        doc="Total number of malformed packets received during the current scan",
    )
    def malformed_packets(self):
        """
        Return the number of malformed packets
        """
        return self._malformed_packets

    @attribute(
        dtype="DevFloat",
        polling_period=5000,
        doc="Time average of all relative weights for the current scan",
    )
    def relative_weight(self):
        """
        Return the average, over time and frequency, of all relative weights
        """
        return self._relative_weight

    @attribute(
        dtype=("DevFloat",),
        polling_period=5000,
        max_dim_x=1024,
        min_value=0,
        doc="Time average of relative weights for each channel in the current scan",
    )
    def relative_weights(self):
        """
        Return the average, over time, of all relative weights for each channel
        """
        return self._relative_weights

    # --------
    # Commands
    # --------

    # def init_command_objects(self):
    #     """
    #     Set up the handler objects for Commands
    #     """
    #     super().init_command_objects()

    class InitCommand(PstSoftwareDevice.InitCommand):
        """
        Class that implements device initialisation for RECV
        State is managed under the hood; the basic sequence is:

        1. Device state is set to INIT
        2. The do() method is run
        3. Device state is set to the appropriate outgoing state,
           usually off

        """

        def do(self):

            """Initialises the attributes and properties"""
            super().do()
            device = self.target

            device._received_rate = 0
            device._received_data = 0
            device._dropped_rate = 0
            device._dropped_data = 0
            device._misordered_packets = 0
            device._malformed_packets = 0
            device._relative_weight = 0.0
            device._relative_weights = (0,)

            device._nchan = 0

            # TODO launch the RECV container
            device.logger.info("RECV init_device complete")
            return (ResultCode.OK, "Init command succeeded")

    def create_beam(self, argin, argout):
        """
        Configure the RECV software contariner based on the JSON arguments
        """
        self.logger.info("Creating beam with argin=%s argout=%s", argin, argout)
        if self._simulation_mode == SimulationMode.TRUE:
            self._configure_simulation(argin)
        return (ResultCode.OK, "command succeeded")

    def set_mode(self, argin, argout):
        """
        Connect to RECV container and issue set_mode command
        """
        self.logger.debug("Setting mode with argin=%s argout=%s", argin, argout)
        return (ResultCode.OK, "command succeeded")

    def update_subint(self, argin, argout):
        """
        Connect to RECV container and issue update_subint command
        """
        self.logger.debug("Updating subint with argin=%s argout=%s", argin, argout)
        return (ResultCode.OK, "command succeeded")

    def stop(self, argin, argout):
        """
        Connect to RECV container and issue stop command
        """
        self.logger.debug("Stopping with argin=%s argout=%s", argin, argout)
        return (ResultCode.OK, "command succeeded")

    def terminate(self, argin, argout):
        """
        Connect to RECV container and issue terminate command
        """
        self.logger.debug("Terminating with argin=%s argout=%s", argin, argout)
        return (ResultCode.OK, "command succeeded")

    def delete_beam(self, argin, argout):
        """
        Connect to the RECV containers and stop the RECV processes.
        """
        self.logger.debug("Deleting beam with argin=%s argout=%s", argin, argout)
        return (ResultCode.OK, "command succeeded")

    def self_test(self, argin, argout):
        """
        Connect to RECV containers and initiate self-testing
        """
        self.logger.debug("starting self test with argin=%s argout=%s", argin, argout)
        return (ResultCode.OK, "command succeeded")

    def _configure_simulation(self, argin):
        """
        Configure parameters required for simulation of RECV
        """
        self.logger.debug("configuring simulation with argin=%s", argin)
        self._nchan = randint(128, 1024)
        self._relative_weights = [0] * self._nchan

    def _simulate_recv(self):
        """
        Simulate attribute changes for RECV when running in simulation mode
        """
        if self._nchan == 0:
            self._configure_simulation({})

        self._received_rate = randint(0, 90)
        self._received_data += int(self._received_rate * 1e9 / 8)
        self._dropped_rate = 0
        self._dropped_data += int(self._dropped_rate * 1e9 / 8)
        self._misordered_packets = randint(0, 3)
        self._malformed_packets = randint(0, 3)
        for i in range(self._nchan):
            self._relative_weights[i] = randint(0, 128)
        self._relative_weight = sum(self._relative_weights) / self._nchan


# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    """Main function of the PstRecv module."""
    return PstRecv.run_server(args=args, **kwargs)


if __name__ == "__main__":
    main()
