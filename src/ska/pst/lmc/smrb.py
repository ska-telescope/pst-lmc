# -*- coding: utf-8 -*-
#
# This file is part of the PstSmrb project
#
#
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" SKA1 Pulsar Timing Sub-element

Device modelling the Pst Share Memory Ring Buffer software component
"""

# PyTango imports
# pylint: disable=unused-import,duplicate-code,no-member
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
import enum
import json
from numpy.random import normal
from random import randint

# pylint: enable=unused-import,duplicate-code

# Additional import
# PROTECTED REGION ID(PstSmrb.additionnal_import) ENABLED START #
from ska.base.control_model import SimulationMode
from ska.base.commands import ResultCode
from ska.pst.lmc.software_device import PstSoftwareDevice

# PROTECTED REGION END #    //  PstSmrb.additionnal_import

__all__ = ["PstSmrb", "main"]


class PstSmrb(PstSoftwareDevice):
    """
    Device modelling the Pst Share Memory Ring Buffer software component

    **Properties:**

    - Device Property
    """

    # -----------------
    # Device Properties
    # -----------------

    # ----------
    # Attributes
    # ----------

    _nsubbands = 0
    _subband_ring_buffer_sizes = []
    _subband_ring_buffer_utilisations = []
    _ring_buffer_size = 0
    _ring_buffer_utilisation = 0

    # ---------------
    # General methods
    # ---------------

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        state = self.get_state()

        if state in [DevState.OFF or DevState.INIT]:
            self.logger.debug("No action since state is off/init")
        else:
            if self._simulation_mode == SimulationMode.TRUE:
                self._simulate_smrb()
            else:
                argout = self._control_socket.json_command("GetState", "{}")
                out = json.loads(argout)
                self._nsubbands = out["nsubbands"]
                self._subband_ring_buffer_sizes = out["subband_ring_buffer_sizes"]
                self._subband_ring_buffer_utilisations = out[
                    "subband_ring_buffer_utilisations"
                ]
                self._ring_buffer_size = sum(self._subband_ring_buffer_sizes)
                self._ring_buffer_utilisation = sum(
                    self._subband_ring_buffer_utilisations
                )

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """

    # ------------------
    # Attributes methods
    # ------------------

    @attribute(
        dtype="DevFloat",
        label="Utilisation",
        unit="Percentage",
        display_unit="%",
        polling_period=5000,
        max_value=100,
        min_value=0,
        doc="Percentage of ring buffer elements full of data",
    )
    def ring_buffer_utilisation(self):
        """
        Return the ring buffer utilisation "
        """
        return self._ring_buffer_utilisation

    @attribute(
        dtype="DevULong64",
        label="Capacity",
        unit="Bytes",
        standard_unit="Bytes",
        display_unit="B",
        polling_period=5000,
        doc="Capacity of ring buffer in bytes",
    )
    def ring_buffer_size(self):
        """Return the ring_buffer_size attribute."""
        return self._ring_buffer_size

    @attribute(
        dtype="DevUShort",
        polling_period=5000,
        doc="Number of sub-bands",
    )
    def number_subbands(self):
        """Return the number_subbands attribute."""
        return self._nsubbands

    @attribute(
        dtype=("DevFloat",),
        max_dim_x=4,
        unit="Percentage",
        standard_unit="Percentage",
        display_unit="%",
        max_value=100,
        min_value=0,
        max_alarm=90,
        min_alarm=-1,
        max_warning=80,
        min_warning=-1,
        doc="Percentage of full ring buffer elements for each sub-band",
    )
    def subband_ring_buffer_utilisations(self):
        """Return the subband_ring_buffer_utilisations attribute."""
        return self._subband_ring_buffer_utilisations

    @attribute(
        dtype=("DevFloat",),
        max_dim_x=4,
        label="Sub-band ring buffer sizes",
        unit="Bytes",
        standard_unit="Bytes",
        display_unit="b",
        min_value=0,
        doc="Capacity of ring buffers, in bytes, for each sub-band",
    )
    def subband_ring_buffer_sizes(self):
        """Return the subband_ring_buffer_sizes attribute."""
        return self._subband_ring_buffer_sizes

    # --------
    # Commands
    # --------

    # def init_command_objects(self):
    #     """
    #     Set up the handler objects for Commands
    #     """
    #     super().init_command_objects()

    class InitCommand(PstSoftwareDevice.InitCommand):
        """
        Class that implements device initialisation for the SMRB
        State is managed under the hood; the basic sequence is:

        1. Device state is set to INIT
        2. The do() method is run
        3. Device state is set to the appropriate outgoing state,
           usually off

        """

        def do(self):

            """Initialises the attributes and properties of the SMRB."""
            super().do()
            device = self.target

            device._ring_buffer_utilisation = 0
            device._ring_buffer_size = 0
            device._nsubbands = 0
            device._subband_ring_buffer_utilisations = (0,)
            device._subband_ring_buffer_sizes = (0,)

            device.logger.info("SMRB init_device complete")
            return (ResultCode.OK, "Init command succeeded")

    def create_beam(self, argin, argout):
        """
        Configure the SMRB model of the TANGO device based on the JSON arguments
        argin command and argout response
        """
        self.logger.info("Creating beam with argin=%s argout=%s", argin, argout)
        if self._simulation_mode == SimulationMode.TRUE:
            self._configure_simulation(argin)

        return (ResultCode.OK, "Command succeeded")

    def set_mode(self, argin, argout):
        """
        Send the set_mode command to the SMRB container
        """
        self.logger.debug("Setting mode with argin=%s argout=%s", argin, argout)
        return (ResultCode.OK, "Command succeeded")

    def update_subint(self, argin, argout):
        """
        Send the update_subint command to the SMRB container
        """
        self.logger.debug("Updating subint with argin=%s argout=%s", argin, argout)
        return (ResultCode.OK, "Command succeeded")

    def stop(self, argin, argout):
        """
        Send the stop command to the SMRB container
        """
        self.logger.debug("Stopping with argin=%s argout=%s", argin, argout)
        return (ResultCode.OK, "Command succeeded")

    def terminate(self, argin, argout):
        """
        Send the terminate command to the SMRB container
        """
        self.logger.debug("Terminating with argin=%s argout=%s", argin, argout)
        return (ResultCode.OK, "Command succeeded")

    def delete_beam(self, argin, argout):
        """
        Connect to the SMRB container and delete the SMRBs.
        :return:None
        """
        self.logger.debug("Deleting beam with argin=%s argout=%s", argin, argout)
        return (ResultCode.OK, "Command succeeded")

    def self_test(self, argin, argout):
        """
        Check the SMRB container is running
        Try to allocate the maximum amount of memory in a ring buffer
        Write to, then read from the ring buffer
        Delete the ring buffer
        """
        self.logger.debug("starting self test with argin=%s argout=%s", argin, argout)
        return (ResultCode.OK, "Command succeeded")

    def _configure_simulation(self, argin):
        """
        Configure simulation parameters
        """
        self.logger.debug("configuring simulation with argin=%s", argin)
        self._nsubbands = randint(1, 4)
        self._subband_ring_buffer_sizes = [0] * self._nsubbands
        self._subband_ring_buffer_utilisations = [0] * self._nsubbands
        for i in range(self._nsubbands):
            self._subband_ring_buffer_sizes[i] = 1048576 * randint(4, 64)
        self._ring_buffer_size = sum(self._subband_ring_buffer_sizes)

    def _simulate_smrb(self):
        if self._nsubbands == 0:
            self._configure_simulation({})
        for i in range(self._nsubbands):
            self._subband_ring_buffer_utilisations[i] = randint(0, 79)
        total = sum(self._subband_ring_buffer_utilisations)
        self._ring_buffer_utilisation = total / self._nsubbands


# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    """Main function of the PstSmrb module."""
    return PstSmrb.run_server(args=args, **kwargs)


if __name__ == "__main__":
    main()
