# -*- coding: utf-8 -*-
#
# This file is part of the PstBeam project
#
# Swinburne University of Technology
#
# Distributed under the terms of the none license.
# See LICENSE.txt for more info.

""" Pst LMC

Provides the Beam Capability for the Pulsar Timing Sub-element
"""

# PyTango imports
# Additional import
# PROTECTED REGION ID(PstBeam.additionnal_import) ENABLED START #
# pylint: disable=unused-import
# pylint: disable=no-member
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
import enum

# pylint: enable=unused-import
from ska.base import SKACapability, SKABaseDevice

# from ska.base.control_model import ObsState
from ska.base.commands import ResponseCommand, ResultCode

# PROTECTED REGION END #    //  PstBeam.additionnal_import

__all__ = ["PstBeam", "main"]


class PstBeam(SKACapability):
    """
    Provides the Beam Capability for the Pulsar Timing Sub-element

    **Properties:**

    - Device Property
        beam_id
            - Type:'DevString'
        RecvFQDN
            - Tango Device for the RECV software component
            - Type:'DevString'
        SmrbFQDN
            - Tango Device for the SMRB software component
            - Type:'DevString'
        DspFQDN
            - Tango Device for the DSP software component
            - Type:'DevString'
        StatFQDN
            - Tango Device for the STAT software component
            - Type:'DevString'
        SendFQDN
            - Tango Device for the SEND software component
            - Type:'DevString'
        MgmtFDQN
            - Tango Device for the MGMT software component
            - Type:'DevString'
        BeamSrvFQDN
            - Tango Device for Beam Server Hardware Component
            - Type:'DevString'
    """

    # PROTECTED REGION ID(PstBeam.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  PstBeam.class_variable

    # -----------------
    # Device Properties
    # -----------------

    beam_id = device_property(
        dtype="DevString",
    )

    RecvFQDN = device_property(
        dtype="DevString",
    )

    SmrbFQDN = device_property(
        dtype="DevString",
    )

    DspFQDN = device_property(
        dtype="DevString",
    )

    StatFQDN = device_property(
        dtype="DevString",
    )

    SendFQDN = device_property(
        dtype="DevString",
    )

    MgmtFDQN = device_property(
        dtype="DevString",
    )

    BeamSrvFQDN = device_property(
        dtype="DevString",
    )

    # ----------
    # Attributes
    # ----------
    _software_version = ""
    _channel_block_number = 0
    _cease_scan = False
    _channel_block_mac_addresses = ("",)
    _channel_block_ip_addresses = ("",)
    _channel_block_ports = (0,)
    _components = []

    # ---------------
    # General methods
    # ---------------

    class InitCommand(SKACapability.InitCommand):
        """
        Command class for device initialisation
        """

        def do(self):
            """
            Stateless hook for initialisation of the attributes and
            properties of the PstBeam.
            """
            import ska.pst.lmc.release as release

            (result_code, message) = super().do()

            device = self.target
            device._build_state = release.get_release_info()
            device._version_id = release.VERSION

            device._software_version = release.VERSION
            device._channel_block_number = 0
            device._cease_scan = False
            device._channel_block_mac_addresses = ("",)
            device._channel_block_ip_addresses = ("",)
            device._channel_block_ports = (0,)

            device.logger.info("Adding Smrb, Recv and Dsp to _components")
            device._components = []
            device._components.append({"name": "Smrb", "fqdn": device.SmrbFQDN})
            device._components.append({"name": "Recv", "fqdn": device.RecvFQDN})
            device._components.append({"name": "Dsp", "fqdn": device.DspFQDN})

            device.set_change_event("adminMode", True, True)
            device.set_archive_event("adminMode", True, True)

            return (result_code, message)

    def init_command_objects(self):
        """
        Initialises the command handlers for commands supported by this
        device.
        """

        self.logger.info("super().init_command_objects()")
        super().init_command_objects()

        args = (self, self.state_model, self.logger)

        self.logger.info("Registering PST specific commands")
        self.register_command_object("On", self.OnCommand(*args))
        self.register_command_object("Off", self.OffCommand(*args))
        self.register_command_object("Reset", self.OffCommand(*args))
        self.register_command_object("SetMode", self.SetModeCommand(*args))
        self.register_command_object("UpdateSubint", self.UpdateSubintCommand(*args))
        self.register_command_object("Stop", self.StopCommand(*args))
        self.register_command_object("Terminate", self.TerminateCommand(*args))

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """

    # ------------------
    # Attributes methods
    # ------------------

    @attribute(
        dtype="DevString",
        doc="Docker container identifier for PST software version.",
    )
    def softwareVersion(self):
        """
        Return the software_version attribute.
        """
        return self._software_version

    @softwareVersion.write
    def softwareVersion(self, value):
        """
        Set the softwareVersion attribute

        :param value: the new software version
        :type value: str
        """
        self.logger.debug("setting software version to %s", value)
        self._software_version = value

    @attribute(
        dtype="DevULong",
        doc="Channel block number.",
    )
    def channelBlockNumber(self):
        """
        Return the channel_block_number attribute.
        """
        return self._channel_block_number

    @attribute(
        dtype="DevBoolean",
        polling_period=5000,
    )
    def ceaseScan(self):
        """
        Return the cease_scan attribute.
        """
        return self._cease_scan

    @attribute(
        dtype=("DevString",),
        max_dim_x=64,
        format="%s",
        doc="List of the MAC address for each channel block",
    )
    def channelBlockMacAddresses(self):
        """
        Return the channel_block_mac_addresses attribute.
        """
        return self._channel_block_mac_addresses

    @attribute(
        dtype=("DevString",),
        max_dim_x=64,
        format="%s",
        doc="List of the IPv4 address for each channel block ",
    )
    def channelBlockIPAddresses(self):
        """
        Return the channel_block_mac_addresses attribute.
        """
        return self._channel_block_ip_addresses

    @attribute(
        dtype=("DevUShort",),
        max_dim_x=64,
        format="%i",
        doc="List of the UDP ports for each channel block",
    )
    def channelBlockPorts(self):
        """
        Return the channel_block_mac_addresses attribute.
        """
        return self._channel_block_ports

    # --------
    # Commands
    # --------

    class SetModeCommand(ResponseCommand):
        """
        Command class for the SetMode() command
        """

        def do(self, argin=None):
            """
            Stateless hook implementing the functionality of the
            SetMode command

            :param argin: Configuration specification
            :type argin: JSON str
            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            :rtype: (ResultCode, str)
            """
            device = self.target
            device.logger.info("setting mode with argin=%s", argin)

            # device.logger.info("Changing obs_state to CONFIGURING")
            # device.state_model._set_obs_state(ObsState.CONFIGURING)

            # if required, send a create_beam command to child devices
            # send the set_mode command to the device proxies
            for component in device._components:
                proxy = tango.DeviceProxy(component["fqdn"])
                (result_code, message) = proxy.command_inout("CreateBeam", argin)
                if result_code != ResultCode.OK:
                    device.logger.warn(
                        "CreateBeam command failed on %s: %s",
                        component["name"],
                        message,
                    )
                    return (ResultCode.FAILED, message)

            for component in device._components:
                proxy = tango.DeviceProxy(component["fqdn"])
                (result_code, message) = proxy.command_inout("SetMode", argin)
                if result_code != ResultCode.OK:
                    device.logger.warn(
                        "SetMode command failed on %s: %s", component["name"], message
                    )
                    return (ResultCode.FAILED, message)

            # device.logger.info("Changing obs_state to SCANNING")
            # device.state_model._set_obs_state(ObsState.SCANNING)

            return (ResultCode.OK, "SetMode command completed successfully")

    @command(
        dtype_in="DevString",
        doc_in="configuration parameter set encoded as JSON string",
        dtype_out="DevVarLongStringArray",
        doc_out="(ReturnType, 'informational message')",
    )
    @DebugIt()
    def SetMode(self, argin):
        """
        Configure the beam as specified in the configuration parameters
        encoded in the JSON string.
        """
        self.logger.debug("setting mode with argin=%s", argin)
        command = self.get_command_object("SetMode")
        (return_code, message) = command(argin)
        return [[return_code], [message]]

    class UpdateSubintCommand(ResponseCommand):
        """
        Command class for the UpdateSubint() command
        """

        def do(self, argin=None):
            """
            Stateless hook implementing the functionality of the
            UpdateSubint command.

            :param argin: Configuration specification
            :type argin: JSON str
            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            :rtype: (ResultCode, str)
            """
            device = self.target

            device.logger.debug("updating subint with argin=%s", argin)

            for component in device._components:
                proxy = tango.DeviceProxy(component["fqdn"])
                (result_code, message) = proxy.command_inout("UpdateSubint", argin)
                if result_code != ResultCode.OK:
                    device.logger.warn(
                        "UpdateSubint command failed on %s: %s",
                        component["name"],
                        message,
                    )
                    return (ResultCode.FAILED, message)

            # device.logger.debug("Changing obs_state to SCANNING")
            # device.state_model._set_obs_state(ObsState.SCANNING)

            return (ResultCode.OK, "UpdateSubint command completed successfully")

    @command(
        dtype_in="DevString",
        doc_in="Configuration parameters encoded in JSON string",
        dtype_out="DevVarLongStringArray",
        doc_out="(ReturnType, 'informational message')",
    )
    @DebugIt()
    def UpdateSubint(self, argin):
        """
        Update the beam configuration during a scan
        """
        self.logger.debug("updating subint with argin=%s", argin)
        command = self.get_command_object("UpdateSubint")
        (return_code, message) = command()
        return [[return_code], [message]]

    class StopCommand(ResponseCommand):
        """
        Command class for the Stop() command
        """

        def do(self, argin=None):
            """
            Stateless hook implementing the functionality of the
            Stop command.

            :param argin: Configuration specification
            :type argin: JSON str
            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            :rtype: (ResultCode, str)
            """
            device = self.target

            device.logger.debug("stopping scan with argin=%s", argin)

            for component in device._components:
                proxy = tango.DeviceProxy(component["fqdn"])
                (result_code, message) = proxy.command_inout("Stop", argin)
                if result_code != ResultCode.OK:
                    device.logger.warn(
                        "Stop command failed on %s: %s", component["name"], message
                    )
                    return (ResultCode.FAILED, message)

            # device.logger.debug("Changing obs_state to READY")
            # device.state_model._set_obs_state(ObsState.READY)

            return (ResultCode.OK, "Stop command completed successfully")

    @command(
        dtype_in="DevString",
        doc_in="Configuration parameters encoded in JSON string",
        dtype_out="DevVarLongStringArray",
        doc_out="(ReturnType, 'informational message')",
    )
    @DebugIt()
    def Stop(self, argin):
        """
        End the current scan and transition the Beam to the READY Operational State.
        """
        self.logger.debug("argin=%s", argin)
        command = self.get_command_object("Stop")
        (return_code, message) = command(argin)
        return [[return_code], [message]]

    class TerminateCommand(ResponseCommand):
        """
        Command class for the Terminate() command
        """

        def do(self, argin=None):
            """
            Stateless hook implementing the functionality of the
            Terminate command.

            :param argin: Configuration specification
            :type argin: JSON str
            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            :rtype: (ResultCode, str)
            """
            device = self.target
            device.logger.debug("deconfiguring beam with argin=%s", argin)

            device.logger.info("calling Terminate command on proxies")
            for component in device._components:
                proxy = tango.DeviceProxy(component["fqdn"])
                (result_code, message) = proxy.command_inout("Terminate", argin)
                if result_code != ResultCode.OK:
                    device.logger.warn(
                        "Terminate command failed on %s: %s", component["name"], message
                    )
                    return (ResultCode.FAILED, message)

            return (ResultCode.OK, "Terminate command completed successfully")

    @command(
        dtype_in="DevString",
        doc_in="Configuration parameters encoded as JSON string",
        dtype_out="DevVarLongStringArray",
        doc_out="(ReturnType, 'informational message')",
    )
    @DebugIt()
    def Terminate(self, argin):
        """
        Deconfigure the Beam, returning to the IDLE Operational State.
        """
        self.logger.debug("argin=%s", argin)
        command = self.get_command_object("Terminate")
        (return_code, message) = command(argin)
        return [[return_code], [message]]

    class OnCommand(SKABaseDevice.OnCommand):
        """
        Command class for the On() command
        """

        def do(self):
            """
            Stateless hook implementing the functionality of the
            On command.

            Switch on the PST devices allocated to the PST beam. The command is
            executed if the AdminMode is ONLINE or MAINTENANCE. If the
            AdminMode is OFFLINE, NOT-FITTED or RESERVED, the method
            throws an exception

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            :rtype: (ResultCode, str)
            """
            device = self.target
            device.is_On_allowed()

            device.logger.info("calling On command on proxies")
            for component in device._components:
                fqdn = component["fqdn"]
                device.logger.debug("proxy = tango.DeviceProxy(" + fqdn + ")")
                proxy = tango.DeviceProxy(fqdn)
                device.logger.debug(str(proxy) + ".command_inout(On)")
                (result_code, message) = proxy.command_inout("On")
                device.logger.debug(
                    "result_code=" + str(result_code) + " message=" + str(message)
                )
                if result_code != ResultCode.OK:
                    device.logger.warn(
                        "On command failed on %s: %s", component["name"], message
                    )
                    return (ResultCode.FAILED, message)

            (result_code, message) = super().do()
            return (result_code, message)

    class OffCommand(SKABaseDevice.OffCommand):
        """
        Command class for the Off() command
        """

        def do(self):
            """
            Stateless hook implementing the functionality of the
            Off command.

            Switch off all the PST devices associated with the PST beam.

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            :rtype: (ResultCode, str)
            """
            device = self.target
            device.is_Off_allowed()

            device.logger.info("calling Off command on proxies")
            for component in device._components:
                proxy = tango.DeviceProxy(component["fqdn"])
                (result_code, message) = proxy.command_inout("Off")
                if result_code != ResultCode.OK:
                    device.logger.warn(
                        "Off command failed on %s: %s", component["name"], message
                    )
                    return (ResultCode.FAILED, message)

            (result_code, message) = super().do()

            return (result_code, message)

    class ResetCommand(SKABaseDevice.ResetCommand):
        """
        Command class for the Reset() command
        """

        def do(self):
            """
            Stateless hook implementing the functionality of the
            Reset command.

            Switch off all the PST devices associated with the PST beam.

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            :rtype: (ResultCode, str)
            """
            device = self.target
            device.is_Reset_allowed()

            device.logger.info("calling Reset command on proxies")
            for component in device._components:
                proxy = tango.DeviceProxy(component["fqdn"])
                (result_code, message) = proxy.command_inout("Reset")
                if result_code != ResultCode.OK:
                    device.logger.warn(
                        "Reset command failed on %s: %s", component["name"], message
                    )
                    return (ResultCode.FAILED, message)

            (result_code, message) = super().do()

            return (result_code, message)


# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    """Main function of the PstBeam module."""
    return PstBeam.run_server(args=args, **kwargs)


if __name__ == "__main__":
    main()
