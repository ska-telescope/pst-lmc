# -*- coding: utf-8 -*-
#
# This file is part of the PstSoftwareDevice project
#
#
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" SKA Pulsar Timing Sub-element

Simple TCP socket for use with Pst Software Devices
"""

__all__ = ["ControlSocket"]

import json
import socket


class ControlSocket:
    """
    Simple TCP socket for sending JSON commands and receiving JSON replies.
    """

    def __init__(self, host, port, timeout=1):
        """
        Configure the server socket to connect to.
        """
        self.address = (host, port)
        self.timeout = timeout
        self.connected = False
        self.sock = None

    def connect(self):
        """
        Create a socket and connect to the configured address.
        """
        if self.connected:
            return
        self.sock = socket.create_connection(self.address, self.timeout)
        self.connected = True

    def _send(self, data):
        """
        Send the data to the socket.
        """
        self.connect()
        bytes_sent = self.sock.send(data)
        return bytes_sent

    def _recv(self, max_recv=32768):
        """
        Receive data from the socket
        """
        data = self.sock.recv(max_recv)
        return data

    def request_reponse(self, request):
        """
        Send a request to the socket, and receive a response
        """
        self.connect()
        self._send(request)
        return self._recv()

    def json_command(self, command, argin):
        """
        Encode the command into the JSON argin and send to the control socket,
        receive the JSON reply
        """
        decoded = json.loads(argin)
        decoded["COMMAND"] = command
        encoded = json.dumps(decoded)
        response = self.request_reponse(encoded.encode("utf-8"))
        return response.decode("utf-8")

    def close(self):
        """
        Close the socket
        """
        self.sock.close()
        self.connected = False
