# -*- coding: utf-8 -*-
#
# This file is part of the PstDsp project
#
#
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" PST LMC

"""

# pylint: disable=no-member

# PyTango imports
from tango import DevState
from tango.server import attribute
from numpy.random import uniform
from random import randint
import json

# Additional import
from ska.base.control_model import SimulationMode
from ska.base.commands import ResultCode
from ska.pst.lmc.software_device import PstSoftwareDevice

__all__ = ["PstDsp", "main"]


class PstDsp(PstSoftwareDevice):
    """

    **Properties:**

    - Device Property
    """

    # -----------------
    # Device Properties
    # -----------------

    # ----------
    # Attributes
    # ----------

    _nsubbands = 0
    _nchan = 0

    _subband_utilisation = []
    _utilisation = 0
    _output_rfi_spectrum = []
    _output_rfi = 0

    # ---------------
    # General methods
    # ---------------

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        state = self.get_state()
        if state in [DevState.OFF or DevState.INIT]:
            self.logger.debug("No action since state is off/init")
        else:
            if self._simulation_mode == SimulationMode.TRUE:
                self._simulate_dsp()
            else:
                argout = self._control_socket.json_command("GetState", "{}")
                out = json.loads(argout)
                self._subband_utilisation = out["subband_utilisation"]
                self._utilisation = out["utilisation"]
                self._output_rfi_spectrum = out["output_rfi_spectrum"]
                self._output_rfi = out["output_rfi"]

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """

    # ------------------
    # Attributes methods
    # ------------------

    @attribute(
        dtype="DevFloat",
        polling_period=5000,
    )
    def utilisation(self):
        """Return the utilisation attribute."""
        return self._utilisation

    @attribute(
        dtype="DevFloat",
        unit="percentage",
        display_unit="%",
        polling_period=5000,
        max_value=100,
        min_value=0,
        max_alarm=90,
        max_warning=50,
        doc="Percentage of output data flagged as RFI by DSP",
    )
    def output_rfi(self):
        """Return the output_rfi attribute."""
        return self._output_rfi

    @attribute(
        dtype=("DevFloat",),
        polling_period=5000,
        max_dim_x=4,
    )
    def subband_utilisation(self):
        """Return the subband_utilisation attribute."""
        return self._subband_utilisation

    @attribute(
        dtype=("DevFloat",),
        max_dim_x=4096,
        label="Output Channel RFI Percentage",
        unit="percentage",
        standard_unit="percentage",
        display_unit="%",
        polling_period=5000,
        doc="Percentage of data flagged as RFI for each output channel, "
        + "averaeged over each sub-integration",
    )
    def output_rfi_spectrum(self):
        """Return the output_rfi_spectrum attribute."""
        return self._output_rfi_spectrum

    # --------
    # Commands
    # --------

    # def init_command_objects(self):
    #    """
    #    Set up the handler objects for Commands
    #    """
    #    super().init_command_objects()

    class InitCommand(PstSoftwareDevice.InitCommand):
        """
        Class that implements device initialisation for the DSP
        State is managed under the hood; the basic sequence is:

        1. Device state is set to INIT
        2. The do() method is run
        3. Device state is set to the appropriate outgoing state,
           usually off

        """

        def do(self):

            """Initialises the attributes and properties of the DSP."""
            super().do()
            device = self.target

            device._num_subbands = 0
            device._nchan = 0

            device._utilisation = 0.0
            device._output_rfi = 0.0
            device._subband_utilisation = (0.0,)
            device._output_rfi_spectrum = (0.0,)

            return (ResultCode.OK, "Init command succeeded")

    def create_beam(self, argin, argout):
        """
        Send create_beam command to DSP container
        """
        self.logger.debug("Creating beam with argin=%s argout=%s", argin, argout)
        if self._simulation_mode == SimulationMode.TRUE:
            self._configure_simulation(argin)
        return (ResultCode.OK, "Command succeeded")

    def set_mode(self, argin, argout):
        """
        Send set_mode command to DSP container
        """
        self.logger.debug("Setting mode with argin=%s argout=%s", argin, argout)
        return (ResultCode.OK, "Command succeeded")

    def update_subint(self, argin, argout):
        """
        Send update_subint command to DSP container
        """
        self.logger.debug("Updating subint with argin=%s argout=%s", argin, argout)
        return (ResultCode.OK, "Command succeeded")

    def stop(self, argin, argout):
        """
        Send stop command to DSP container
        """
        self.logger.debug("Stopping with argin=%s argout=%s", argin, argout)
        return (ResultCode.OK, "Command succeeded")

    def terminate(self, argin, argout):
        """
        Send terminate command to DSP container
        """
        self.logger.debug("Terminating with argin=%s argout=%s", argin, argout)
        return (ResultCode.OK, "Command succeeded")

    def delete_beam(self, argin, argout):
        """
        Send delete_beam command to DSP container
        """
        self.logger.debug("Deleting beam with argin=%s argout=%s", argin, argout)
        return (ResultCode.OK, "Command succeeded")

    def self_test(self, argin, argout):
        """
        Send self_test command to DSP container
        """
        self.logger.debug("starting self test with argin=%s argout=%s", argin, argout)
        return (ResultCode.OK, "Command succeeded")

    def _configure_simulation(self, argin):
        """
        Generate random configuration for simulated behaviour
        """
        self.logger.debug("configuring simulation with argin=%s", argin)
        self._nsubbands = randint(1, 4)
        self._nchan = randint(64, 1024)
        self._subband_utilisation = [0.0] * self._nsubbands
        self._output_rfi_spectrum = [0.0] * self._nchan

    def _simulate_dsp(self):
        """
        Simulate a DSP container"
        """
        if self._nsubbands == 0:
            self._configure_simulation({})

        for i in range(self._nsubbands):
            self._subband_utilisation[i] = uniform(0, 49)
        for i in range(self._nchan):
            self._output_rfi_spectrum[i] = uniform(0, 5)
        self._output_rfi = sum(self._output_rfi_spectrum) / self._nchan
        self._utilisation = sum(self._subband_utilisation) / self._nsubbands

        self.logger.debug(
            "Utilisation=%f Average=%f", self._subband_utilisation, self._utilisation
        )


# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    """Main function of the PstDsp module."""
    return PstDsp.run_server(args=args, **kwargs)


if __name__ == "__main__":
    main()
