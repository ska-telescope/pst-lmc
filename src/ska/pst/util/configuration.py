# -*- coding: utf-8 -*-
#
# This file is part of the PstBeam project
#
# Swinburne University of Technology
#
# Distributed under the terms of the none license.
# See LICENSE.txt for more info.

from typing import Any


class Configuration(object):
    """Configuration.

    This class represents a PST-LMC configuration that is
    sent from the CSP. It is a generic object that is
    able to validate itself against the `ska-telmodel`
    schema for PST.

    Creating instances of these from a JSON encoded string
    should be done via the `Configration.from_json()` method.
    To convert the object to a JSON encoded string should
    be throught the `to_json()` method.
    """

    def __init__(self, values: dict) -> "Configuration":
        from copy import deepcopy

        if values is None or len(values) == 0:
            raise ValueError("Parameter 'values' must not be empty")

        self._values = deepcopy(values)

    def __getattr__(self, name: str) -> Any:
        return self._values[name]

    def __setattr__(self, name: str, value: Any) -> None:
        if name == "_values":
            self.__dict__["_values"] = value
        else:
            self._values[name] = value

    def __getitem__(self, name: str) -> Any:
        return self._values[name]

    def __delitem__(self, name: str) -> Any:
        del self._values[name]

    def __setitem__(self, name: str, value: Any) -> None:
        self._values[name] = value

    def __len__(self) -> int:
        return len(self._values)

    def keys(self) -> Any:
        return self._values.keys()

    def values(self) -> Any:
        return self._values.values()

    def items(self) -> Any:
        return self._values.items()

    @staticmethod
    def from_json(json_str: str) -> "Configuration":
        """Creates an instance of a Configuration class from
        as JSON encoded string. This will also validate that
        the given string matches what is expected, this is
        performed by calling :py:meth:`ska.pst.util.validate`.

        :param json_str: JSON encode string of a configuration object.
        :returns: A Configuration object.
        :raises: `ValueError` is not a valid configuration request.
        """

        import json
        from .validation import validate

        obj = json.loads(json_str)

        obj = validate(obj)

        return Configuration(values=obj)

    def to_json(self) -> str:
        """Serialises the Configuration object to a JSON string.

        :returns: a JSON encoded string.
        """
        import json

        return json.dumps(self._values)
