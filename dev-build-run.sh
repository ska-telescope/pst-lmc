# optional: remove old image via docker rmi
# (with minikube docker-env set)

# 1.
make build

# 2.
docker save nexus.engageska-portugal.pt/ska-docker/pst-lmc:latest > ~/.minikube/cache/images/nexus.engageska-portugal.pt/ska-docker/pst-lmc_latest
# note cache is configured in a json file
# ~/.minikube/config/config.json


minikube cache reload

#  
make install-chart

