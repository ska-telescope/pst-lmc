# -*- coding: utf-8 -*-
"""
Some simple unit tests of the PstBeamSrvBmc device, exercising the device from
the same host as the tests by using a DeviceTestContext.
"""

from tango import DevState
from tango.test_utils import DeviceTestContext
from ska.pst.lmc import PstBeamSrvBmc

def test_init():
    """Test device goes into STANDBY when initialised"""
    with DeviceTestContext(PstBeamSrvBmc, process=True) as proxy:
        proxy.Init()
        assert proxy.state() == DevState.OFF

def test_on():
    """Test device turns on when requested"""
    with DeviceTestContext(PstBeamSrvBmc, process=True) as proxy:
        proxy.Init()
        proxy.On()
        assert proxy.state() == DevState.ON

def test_off():
    """Test device turns off when requested"""
    with DeviceTestContext(PstBeamSrvBmc, process=True) as proxy:
        proxy.Init()
        proxy.On()
        proxy.Off()
        assert proxy.state() == DevState.OFF
