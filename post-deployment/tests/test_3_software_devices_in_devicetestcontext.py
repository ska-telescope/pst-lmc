# -*- coding: utf-8 -*-
"""
Some simple unit tests of the PstSmrb device, exercising the device from
the same host as the tests by using a DeviceTestContext.
"""

from tango import DevState
from tango.test_utils import DeviceTestContext
from ska.pst.lmc import PstRecv, PstSmrb, PstDsp

devices = [PstRecv, PstSmrb, PstDsp]

def test_init():
    """Test device goes into STANDBY when initialised"""
    for device in devices:
        with DeviceTestContext(device, process=True) as proxy:
            proxy.Init()
            assert proxy.state() == DevState.OFF

def test_on():
    """Test device turns on when requested"""
    for device in devices:
        with DeviceTestContext(device, process=True) as proxy:
            proxy.Init()
            proxy.On()
            assert proxy.state() == DevState.ON
            proxy.Off()

def test_off():
    """Test device turns off when requested"""
    for device in devices:
        with DeviceTestContext(device, process=True) as proxy:
            proxy.Init()
            proxy.On()
            proxy.Off()
            assert proxy.state() == DevState.OFF

def test_create_beam():
    """Test create_beam command works when requested"""
    json_config = '{"NSUBBAND": "1"}'
    for device in devices:
        with DeviceTestContext(device, process=True) as proxy:
            proxy.Init()
            proxy.On()
            proxy.CreateBeam(json_config)
            proxy.DeleteBeam(json_config)
            proxy.Off()

def test_set_mode():
    """Test set_mode command works when requested"""
    json_config = '{"NSUBBAND": "1"}'
    for device in devices:
        with DeviceTestContext(device, process=True) as proxy:
            proxy.Init()
            proxy.On()
            proxy.CreateBeam(json_config)
            proxy.SetMode(json_config)
            proxy.Stop(json_config)
            proxy.DeleteBeam(json_config)
            proxy.Off()

def test_update_subint():
    """Test update_subint command works when requested"""
    json_config = '{"NSUBBAND": "1"}'
    for device in devices:
        with DeviceTestContext(device, process=True) as proxy:
            proxy.Init()
            proxy.On()
            proxy.CreateBeam(json_config)
            proxy.SetMode(json_config)
            proxy.UpdateSubint(json_config)
            proxy.Stop(json_config)
            proxy.DeleteBeam(json_config)
            proxy.Off()

def test_terminate():
    """Test terminate command works when requested"""
    json_config = '{"NSUBBAND": "1"}'
    for device in devices:
        with DeviceTestContext(device, process=True) as proxy:
            proxy.Init()
            proxy.On()
            proxy.CreateBeam(json_config)
            proxy.SetMode(json_config)
            proxy.UpdateSubint(json_config)
            proxy.Stop(json_config)
            proxy.DeleteBeam(json_config)
            proxy.Off()

def test_self_test():
    """Test self_test command works when requested"""
    json_config = '{"NSUBBAND": "1"}'
    for device in devices:
        with DeviceTestContext(device, process=True) as proxy:
            proxy.Init()
            proxy.On()
            proxy.SelfTest(json_config)
            proxy.Off()
