#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

with open('README.md') as readme_file:
    readme = readme_file.read()

setup(
    name='pst-lmc',
    version='0.2.4',
    description="",
    long_description=readme + '\n\n',
    author="Andrew Jameson",
    author_email='ajameson@swin.edu.au',
    url='https://github.com/ska-telescope/pst-lmc',
    packages=find_packages(),
    include_package_data=True,
    license="BSD license",
    zip_safe=False,
    keywords='ska_python_skeleton',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    test_suite='tests',
    install_requires=[
        'lmcbaseclasses >= 0.6.0',
        'pytango <= 9.3.2',
        # "csp-lmc-subelement >= 0.1.2",
    ], 
    setup_requires=[
        # dependency for `python setup.py test`
        'pytest-runner',
        # dependencies for `python setup.py build_sphinx`
        'sphinx',
        'recommonmark'
    ],
    tests_require=[
        'pytest',
        'pytest-cov',
        'pytest-forked',
        'pytest-json-report',
        'pycodestyle',
    ],
    extras_require={
        'dev':  ['prospector[with_pyroma]', 'yapf', 'isort']
    }
)
