ARG DOCKER_REGISTRY_USER
ARG DOCKER_REGISTRY_HOST
FROM ${DOCKER_REGISTRY_HOST}/${DOCKER_REGISTRY_USER}/ska-python-buildenv:9.3.2.1 as buildenv
FROM ${DOCKER_REGISTRY_HOST}/${DOCKER_REGISTRY_USER}/ska-python-runtime:9.3.2.1

# create ipython profile to so that itango doesn't fail if ipython hasn't run yet
RUN ipython profile create

ENV PATH=/home/tango/.local/bin:$PATH
ENV PYTHONPATH=/app/src:/home/tango/.local/lib/python3.7/site-packages

RUN pip install lmcbaseclasses ska-log-transactions

# Not required currently
# USER root
# needed for VM build in LowPSI
# RUN echo 'Acquire::http::Proxy "http://delphinus.atnf.csiro.au:8888"; Acquire::https::Proxy "http://delphinus.atnf.csiro.au:8888";' >> /etc/apt/apt.conf.d/http_proxy.conf
# RUN apt-get update && apt-get install -y ipmitool
# USER tango

# CMD ["/usr/bin/python", "/app/src/ska/pst/lmc/master.py"]
